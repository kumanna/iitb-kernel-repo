# Introduction
This repository has some scripts used by the FOSSEE team at IIT Bombay
for preparing a Debian repository. The repository will hold the kernel
packages as well as other packages and files.

# Instructions
1. Put the packages in the `packages` directory.
2. Edit the `buildrepo.sh` appropriately and execute it.
3. Mirror the `repo` directory to the remote mirror.
4. Add the GPG key to your Debian machine, and add the appropriate APT preferences. An example is in `iitb-kernel-mirror.conf`.
